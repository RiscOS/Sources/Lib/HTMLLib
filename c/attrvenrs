/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "api.h"
#include "internal.h"
#include "elementinf.h"

const char   *HtmlBODYonload         (const HStream * restrict h) { return h->info->body.onload; }
const char   *HtmlBODYonunload       (const HStream * restrict h) { return h->info->body.onunload; }
const char   *HtmlBODYbackground     (const HStream * restrict h) { return h->info->body.background; }
unsigned int  HtmlBODYtext           (const HStream * restrict h) { return h->info->body.text; }
unsigned int  HtmlBODYbgcolour       (const HStream * restrict h) { return h->info->body.bgcolour; }
unsigned int  HtmlBODYlink           (const HStream * restrict h) { return h->info->body.link; }
unsigned int  HtmlBODYvlink          (const HStream * restrict h) { return h->info->body.vlink; }
unsigned int  HtmlBODYalink          (const HStream * restrict h) { return h->info->body.alink; }

const char   *HtmlMETAhttp_equiv     (const HStream * restrict h) { return h->info->meta.http_equiv; }
const char   *HtmlMETAname           (const HStream * restrict h) { return h->info->meta.name; }
const char   *HtmlMETAcontent        (const HStream * restrict h) { return h->info->meta.content; }
const char   *HtmlMETAscheme         (const HStream * restrict h) { return h->info->meta.scheme; }

const char   *HtmlFORMaction         (const HStream * restrict h) { return h->info->form.action; }
formmethod    HtmlFORMmethod         (const HStream * restrict h) { return h->info->form.method; }
const char   *HtmlFORMenctype        (const HStream * restrict h) { return h->info->form.enctype; }
const char   *HtmlFORMaccept_charset (const HStream * restrict h) { return h->info->form.accept_charset; }
const char   *HtmlFORMonreset        (const HStream * restrict h) { return h->info->form.onreset; }
const char   *HtmlFORMonsubmit       (const HStream * restrict h) { return h->info->form.onsubmit; }
const char   *HtmlFORMtarget         (const HStream * restrict h) { return h->info->form.target; }

inputtype     HtmlINPUTtype          (const HStream * restrict h) { return h->info->input.type; }
const char   *HtmlINPUTname          (const HStream * restrict h) { return h->info->input.name; }
const char   *HtmlINPUTvalue         (const HStream * restrict h) { return h->info->input.value; }
bool          HtmlINPUTchecked       (const HStream * restrict h) { return h->info->input.checked; }
bool          HtmlINPUTdisabled      (const HStream * restrict h) { return h->info->input.disabled; }
bool          HtmlINPUTreadonly      (const HStream * restrict h) { return h->info->input.readonly; }
unsigned int  HtmlINPUTsize          (const HStream * restrict h) { return h->info->input.size; }
unsigned int  HtmlINPUTmaxlength     (const HStream * restrict h) { return h->info->input.maxlength; }
const char   *HtmlINPUTsrc           (const HStream * restrict h) { return h->info->input.src; }
const char   *HtmlINPUTalt           (const HStream * restrict h) { return h->info->input.alt; }
const char   *HtmlINPUTusemap        (const HStream * restrict h) { return h->info->input.usemap; }
imgalign      HtmlINPUTalign         (const HStream * restrict h) { return h->info->input.align; }
int           HtmlINPUTtabindex      (const HStream * restrict h) { return h->info->input.tabindex; }
const char   *HtmlINPUTonfocus       (const HStream * restrict h) { return h->info->input.onfocus; }
const char   *HtmlINPUTonblur        (const HStream * restrict h) { return h->info->input.onblur; }
const char   *HtmlINPUTonselect      (const HStream * restrict h) { return h->info->input.onselect; }
const char   *HtmlINPUTonclick       (const HStream * restrict h) { return h->info->input.onclick; }
const char   *HtmlINPUTonchange      (const HStream * restrict h) { return h->info->input.onchange; }
const char   *HtmlINPUTaccept        (const HStream * restrict h) { return h->info->input.accept; }

const char   *HtmlSELECTname         (const HStream * restrict h) { return h->info->select.name; }
unsigned int  HtmlSELECTsize         (const HStream * restrict h) { return h->info->select.size; }
bool          HtmlSELECTmultiple     (const HStream * restrict h) { return h->info->select.multiple; }
bool          HtmlSELECTdisabled     (const HStream * restrict h) { return h->info->select.disabled; }
int           HtmlSELECTtabindex     (const HStream * restrict h) { return h->info->select.tabindex; }
const char   *HtmlSELECTonfocus      (const HStream * restrict h) { return h->info->select.onfocus; }
const char   *HtmlSELECTonblur       (const HStream * restrict h) { return h->info->select.onblur; }
const char   *HtmlSELECTonselect     (const HStream * restrict h) { return h->info->select.onselect; }
const char   *HtmlSELECTonclick      (const HStream * restrict h) { return h->info->select.onclick; }
const char   *HtmlSELECTonchange     (const HStream * restrict h) { return h->info->select.onchange; }
const int    *HtmlSELECToptions      (const HStream * restrict h) { return h->info->select.options; }

bool          HtmlOBJECTdeclare      (const HStream * restrict h) { return h->info->object.declare; }
bool          HtmlOBJECTshapes       (const HStream * restrict h) { return h->info->object.shapes; }
int           HtmlOBJECTtabindex     (const HStream * restrict h) { return h->info->object.tabindex; }
const char   *HtmlOBJECTtype         (const HStream * restrict h) { return h->info->object.type; }
const char   *HtmlOBJECTusemap       (const HStream * restrict h) { return h->info->object.usemap; }
int           HtmlOBJECTvspace       (const HStream * restrict h) { return h->info->object.vspace; }
int           HtmlOBJECThspace       (const HStream * restrict h) { return h->info->object.hspace; }
imgalign      HtmlOBJECTalign        (const HStream * restrict h) { return h->info->object.align; }
const char   *HtmlOBJECTarchive      (const HStream * restrict h) { return h->info->object.archive; }
int           HtmlOBJECTborder       (const HStream * restrict h) { return h->info->object.border; }
const char   *HtmlOBJECTclassid      (const HStream * restrict h) { return h->info->object.classid; }
const char   *HtmlOBJECTcodebase     (const HStream * restrict h) { return h->info->object.codebase; }
const char   *HtmlOBJECTcodetype     (const HStream * restrict h) { return h->info->object.codetype; }
const char   *HtmlOBJECTdata         (const HStream * restrict h) { return h->info->object.data; }
const char   *HtmlOBJECTstandby      (const HStream * restrict h) { return h->info->object.standby; }
HStream      *HtmlOBJECTstream       (const HStream * restrict h) { return h->info->object.hstream; }
HStream      *HtmlOBJECTparent       (const HStream * restrict h) { return h->info->object.parent; }

HStream      *HtmlOBJECTmapstream    (const HStream * restrict h)
{
    if (h->info->object.mapstream)
        return h->info->object.mapstream;

    if (!h->info->object.usemap)
        return NULL;

    return h->info->object.mapstream = __html_find_map(h->info->object.maps,
                                                       h->info->object.usemap);
}

const char   *HtmlPARAMname          (const HStream * restrict h) { return h->info->param.name; }
const char   *HtmlPARAMtype          (const HStream * restrict h) { return h->info->param.type; }
const char   *HtmlPARAMvalue         (const HStream * restrict h) { return h->info->param.value; }
paramtype     HtmlPARAMvaluetype     (const HStream * restrict h) { return h->info->param.valuetype; }

const char *HtmlELEMENTname(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_META:   return h->info->meta.name;
    case TAG_SELECT: return h->info->select.name;
    case TAG_INPUT:  return h->info->input.name;
    case TAG_PARAM:  return h->info->param.name;
    default:         return h->name;
  }
}

int HtmlELEMENTtabindex(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.tabindex;
    case TAG_INPUT:  return h->info->input.tabindex;
    case TAG_OBJECT: return h->info->object.tabindex;
    default:         return 0;
  }
}

const char *HtmlELEMENTonfocus(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.onfocus;
    case TAG_INPUT:  return h->info->input.onfocus;
    default:         return NULL;
  }
}

const char *HtmlELEMENTonblur(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.onblur;
    case TAG_INPUT:  return h->info->input.onblur;
    default:         return NULL;
  }
}

const char *HtmlELEMENTonselect(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.onselect;
    case TAG_INPUT:  return h->info->input.onselect;
    default:         return NULL;
  }
}

const char *HtmlELEMENTonclick(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.onclick;
    case TAG_INPUT:  return h->info->input.onclick;
    default:         return NULL;
  }
}

const char *HtmlELEMENTonchange(const HStream * restrict h)
{
  switch (h->tagno)
  {
    case TAG_SELECT: return h->info->select.onchange;
    case TAG_INPUT:  return h->info->input.onchange;
    default:         return NULL;
  }
}
